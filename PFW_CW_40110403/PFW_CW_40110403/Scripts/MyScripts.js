﻿$(function () {
    //Get page location and mark a tab appropriately with my class "set".
    $loc = $(location).attr('href');

    //For each progress bar on the page calculate the width and assign that value to CSS width.
    $(".container .progress .progress-bar").each(function () {
        var now = parseFloat($(this).attr("aria-valuenow"));
        var max = parseFloat($(this).attr("aria-valuemax"));
        var perc = now / max * 100.0;
        //alert(perc);
        $(this).css('width', perc + '%');
    });

    //Variable used to store post response lated in the code.
    var singed;

    //Determines which page your on and highlight the tab in the nav bar to indicate it.
    if ($loc.indexOf("Cause") > -1) {
        if ($loc.indexOf("startCause") > -1) {
            $("#sacLi a").addClass('set');
        }
        else if ($loc.indexOf("Causes") > -1) {
            $("#myCLi a").addClass('set');
        }
        else
        {
            //Get Cause ID
            var cid = $("#sign_c").attr("value");
            //Execute periodical updates about cause progress, every one minute.
            window.setInterval(getCauseProgress, 60000);

            //Method executed every minute, essentially updates HTML if any changes have occured to the data.
            function getCauseProgress() {
                var cid = $("#sign_c").attr("value");

                $.post("/Home/GetCauseProgress", { cid: cid })
                    .done(function (response, status, jqxhr) {
                        $("#pv_prog").html(response);
                        var now = parseFloat($(".progress-bar").attr("aria-valuenow"));
                        var max = parseFloat($(".progress-bar").attr("aria-valuemax"));
                        var perc = now / max * 100.0;
                        $(".progress-bar").css('width', perc + '%');
                    })
                    .fail(function (jqxhr, status, error) {
                        // this is the ""error"" callback
                        alert(error);
                    })
            }

            //Check if the user viewing the cause has already signed it
            $.post("/Home/CheckSignature", { cid: cid })
                .done(function (response, status, jqxhr) {
                    // this is the "success" callback
                    //alert(response);
                    signed = response;
                    if (signed == "Yes") {
                        $("#sign_c").addClass('disabled');
                    }
                })
                .fail(function (jqxhr, status, error) {
                    // this is the ""error"" callback
                    alert(error)
                })
        }
    }
    else if ($loc.indexOf("Browse") > -1)
    {
        $("#bLi a").addClass('set');
    }
    else if ($loc.indexOf("Search") > -1) {
        $("#sLi a").addClass('set');
    }

    //Code use to handle the button on the home page
    $(".container").on('click', '.sac_btn', function () {
        window.location.href = "/Home/startCause";
    });

    /*
     * Code used to handle the modal used for log in. Also swap the content of a div between register and login depending on the need.
     * The log in page is designed to load as a simple page as well as a modal.
     */
    $(".container").on('click', "#log_in", function () {
        $("#myLogin").modal('show'); 
    });
    $(".container").on('click', "#log_out", function () {
        $.post("/Home/LogOut")
            .done(function (response, status, jqxhr) {
            // this is the "success" callback
            window.location.replace('/Home/Index');

            })
            .fail(function (jqxhr, status, error) {
                // this is the ""error"" callback
                alert(error);
            })

    });
    $(".form-container").on('click', "#registerBtn", function () {
        $(".form-container").load("RegisterPV");
    });
    $(".form-container").on('click', "#loginBtn", function () {
        $(".form-container").load("LoginPV");
    });
    $(".modal-form-container").on('click', "#registerBtn", function () {
        $(".modal-form-container").load("/Home/RegisterPV");
    });
    $(".modal-form-container").on('click', "#loginBtn", function () {
        $(".modal-form-container").load("/Home/LoginPV");
    });

    //Simulates Log in procedure by swaping icons when log in is clicked.
    $(".form-container").on('click', "#myLogInBtn", function () {
        var email = $("#LoginForm #email").val().trim();
        var password = $("#LoginForm #pwd").val().trim();
        $.post("/Home/ValCred", { email: email, password: password })
        .done(function (response, status, jqxhr) {
                // this is the "success" callback
                window.location.replace('/Home/Index');

        })
        .fail(function (jqxhr, status, error) {
                // this is the ""error"" callback
                alert(error);
        })
    });
    $(".modal-form-container").on('click', "#myLogInBtn", function () {
        var email = $("#LoginForm #email").val().trim();
        var password = $("#LoginForm #pwd").val().trim();
        $.post("/Home/ValCred", { email: email, password: password })
            .done(function (response, status, jqxhr) {
                // this is the "success" callback
                if (response == "Logged in") {
                    window.location.replace('/Home/Index');
                }
                else {
                    alert("Please try again");
                }
            })
            .fail(function (jqxhr, status, error) {
                // this is the ""error"" callback
                alert(error);
            })
    });

    //Shows simple hard coded modal used to list people who signed the cause.
    $(".container").on('click', '#sb_btn', function () {
        var cid = $("#sign_c").attr("value");
        $.post("/Home/SignedBy", { cid: cid })
            .done(function (response, status, jqxhr) {
                // this is the "success" callback
                $("#sb_mb").html(response);
            })
            .fail(function (jqxhr, status, error) {
                // this is the ""error"" callback
                alert(error);
            })
        $("#signedByM").modal('show');
    });

    //In the search page this code is used to enable/disable the button based on if there is any cotent in the input.
    $(".container").on('keyup', '#search-input', function () {
        if ($("#search-input").val().trim() != '') {
            $("#srchBtn").removeClass('disabled');
        }
        else {
            $("#srchBtn").addClass('disabled');
        }
    });

    //Tooltip is used to show some info about the button the user is hovering over.
    $(".mynav").tooltip();

    /*
     * Used to alter the progress bar on start a cause page.
     * Get's all input values, trims the whitespaces and evaluates the total completion of the form.
     */
    $('.container').on('keyup', '#sacf input, #sacf textarea', function () {
        var newval = 0;

        var in1 = $("#sacf #title_in").val();
        var in2 = $("#sacf #aim_in").val();
        var in3 = $("#sacf #target_in").val();
        var in4 = $("#sacf textarea").val();

        if ($.trim(in1) != '')
        {
            newval = newval + 20;
        }
        if ($.trim(in2) != '') {
            newval = newval + 20;
        }
        if ($.trim(in3) != '') {
            newval = newval + 20;
        }
        if ($.trim(in4) != '') {
            newval = newval + 40;
        }

        if (newval == 100) {
            $("#sacf button").removeClass('disabled');
        } else {
            $("#sacf button").addClass('disabled');
        }
        $('.progress-bar').css('width', newval + '%').attr('aria-valuenow', newval);
    });

    //Creates a JSON object and populates it with input values. If the values are valid then the data is passed to a ASP.NET function to create a new cause.
    $(".container").on('click', '#crt_cause', function () {
        var cause = new Object();
        cause.Title = $("#sacf #title_in").val();
        cause.DirectedTo = $("#sacf #aim_in").val().trim();
        cause.Description = $("#sacf textarea").val();
        cause.Limit = $("#sacf #target_in").val().trim();
        if (cause.Title != "" && cause.DirectedTo != "" && cause.Description != "" && cause.Limit != "") {
            $.post("/Home/AddCause", cause)
                .done(function (response, status, jqxhr) {
                    // this is the "success" callback
                    alert(response);

                })
                .fail(function (jqxhr, status, error) {
                    // this is the ""error"" callback
                    alert(error);
                })
        }
    });

    //Handles the click of the sign cause button, if there is no error the user is added to the list of people who signed the cause.
    $(".container").on('click', '#sign_c', function () {
        var cid = $("#sign_c").attr("value");
        if (!$('#sign_c').hasClass('disabled')) {
            $.post("/Home/SignCause", { cid: cid })
                .done(function (response, status, jqxhr) {
                    // this is the "success" callback
                    if (response != "Fail") {
                        $("#pv_prog").html(response);
                        var now = parseFloat($(".progress-bar").attr("aria-valuenow"));
                        var max = parseFloat($(".progress-bar").attr("aria-valuemax"));
                        var perc = now / max * 100.0;
                        $(".progress-bar").css('width', perc + '%');
                        $("#sign_c").addClass('disabled');
                    }
                    else {
                        alert("Please log in to sign the cause!");
                    }

                })
                .fail(function (jqxhr, status, error) {
                    // this is the ""error"" callback
                    alert(error);
                })
        }
    });

    //This methods handles the removal of a cause chosen by the Admin.
    $(".container").on('click', '#removeCause', function () {
        var cid = $("#removeCause").attr("value");
        $.post("/Home/RemoveCause", { cid: cid })
            .done(function (response, status, jqxhr) {
                window.location.replace('/Home/Browse');
            })
            .fail(function (jqxhr, status, error) {
                // this is the ""error"" callback
                alert(error);
            })
    });
});