﻿namespace PFW_CW_40110403.Models
{
    public class Admin : User
    {
        //Please note in order to create admin, you have to manually insert it to the database.
        public bool isAdmin = true;
    }
}