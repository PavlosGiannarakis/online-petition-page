﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PFW_CW_40110403.Models
{
    public class DB : DbContext
    {
        //MyContext is the personal connection string in Web.Config. In order to make the web app work locally you have to add a valid
        //connection string in it's place.
        public DB() : base("MyContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DB>());
            //Database.SetInitializer(new DropCreateDatabaseAlways<DB>());
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Cause> Causes { get; set; }
    }
}