﻿namespace PFW_CW_40110403.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //Password is stored as string for demo purposes and due to the fact that security is not a priority.
        public string Password { get; set; }
        public string Email { get; set; }
    }
}