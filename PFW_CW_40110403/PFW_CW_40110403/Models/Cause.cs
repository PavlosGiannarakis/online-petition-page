﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PFW_CW_40110403.Models
{
    [Serializable]
    public class Cause
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        public List<string> DirectedTo { get; set; }
        public List<int> SignedBy { get; set; }
        

        public int Limit { get; set; }

        //Used to store lists in db while also being able to use as lists when required.
        public string DTAsString
        {
            get { return String.Join(", ", DirectedTo); }
            set { DirectedTo = value.Split(',').ToList(); }
        }

        public string SbAsString
        {
            get { return SignedBy == null? null : String.Join(", ", SignedBy); }
            set { SignedBy = value.Split(',').Select(Int32.Parse).ToList(); }
        }
        public byte[] Image { get; set; }
        public virtual User CreatedBy { get; set; }
    }
}