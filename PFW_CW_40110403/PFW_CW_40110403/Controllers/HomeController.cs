﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PFW_CW_40110403.Models;
using System.Web.Script.Serialization;

namespace PFW_CW_40110403.Controllers
{
    public class HomeController : Controller
    {
        //Global database instance.
        DB db = new DB();
        
        //Returns index page with a list of all models for processing.
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                ViewBag.Message = ((User)Session["User"]).Name;
            }
            return View(db.Causes.ToList());
        }

        //Returns a generic page which appears after log in.
        public ActionResult Causes()
        {
            User u = (User)Session["User"];
            return View(u);
        }

        //Get function to display any cause to a web page.
        [HttpGet]
        public ActionResult Cause(int id)
        {
            //Get Cause from db and pass to view.
            Cause c = db.Causes.Find(id);
            if (c == null)
            {
                return RedirectToAction("Browse");
            }
            return View(c);
        }

        //Post method responsible for registering users.
        [HttpPost]
        public ActionResult AddUser( User s)
        {
            db.Users.Add(s);
            db.SaveChanges();
            ViewBag.Message = db.Users.First().Name;
            db.Users.Find(1);
            return RedirectToAction("Index", db.Causes.ToList());
        }

        //Post method to validate credentials for log in.
        [HttpPost]
        public ActionResult ValCred(string email, string password)
        {
            var user = db.Users
                .Where(u => u.Email == email && u.Password == password)
                .Select(u => u).SingleOrDefault();
            int count = db.Users
                .Where(u => u.Email == email && u.Password == password)
                .Select(u => u).Count();
            if (count >= 1)
            {
                Session["User"] = user;
                return Json("Logged in");
            }
            return Json("Failed");
        }

        //Post method to add a cause to the database.
        [HttpPost]
        public ActionResult AddCause(Cause model)
        {
            Cause c = new Cause();

            if (Session["User"] != null)
            {
                int uid = ((User)Session["User"]).Id;
                c.CreatedBy = db.Users.Where(u => u.Id == uid).SingleOrDefault();

                if (model != null)
                {
                    c.Title = model.Title;
                    c.Description = model.Description;
                    c.DirectedTo = model.DirectedTo;
                    c.Limit = Convert.ToInt32(model.Limit);
                    c.SignedBy = new List<int>();
                    c.SignedBy.Add(uid);
                }
                else
                {
                    return Json("Error");
                }

                db.Causes.Add(c);
                db.SaveChanges();
                return Json("Cause created successfully!");
            }
            else {
                return Json("Please Log in to create cause");
            }

        }

        //Post method that allows users to sign causes.
        [HttpPost]
        public ActionResult SignCause(string cid)
        {
            if(Session["User"] != null)
            {
                int c_id = Convert.ToInt32(cid);
                Cause x = db.Causes.Where(c => c.Id == c_id).SingleOrDefault();
                x.SignedBy.Add(((User)Session["User"]).Id);
                db.SaveChanges();
                return PartialView("_CauseProgress", x);
            }
            return Json("Fail");
        }

        //Post methods responsible for returning the latest data on cause signature progress.
        [HttpPost]
        public ActionResult GetCauseProgress(string cid)
        {
            int c_id = Convert.ToInt32(cid);
            Cause x = db.Causes.Where(c => c.Id == c_id).SingleOrDefault();
            return PartialView("_CauseProgress", x);
        }

        //Provides a list of users that signed a cause.
        public ActionResult SignedBy(string cid)
        {
            int c_id = Convert.ToInt32(cid);
            Cause x = db.Causes.Where(c => c.Id == c_id).SingleOrDefault();
            List<User> usersSigned = new List<User>();
            foreach (int id in x.SignedBy)
            {
                usersSigned.Add(db.Users.Where(u => u.Id == id).SingleOrDefault());
            }

            return PartialView("_SignedBy", usersSigned);
        }

        //Method that allows the admin to delete causes.
        [HttpPost]
        public ActionResult RemoveCause(string cid)
        {
            int c_id = Convert.ToInt32(cid);
            Cause x = db.Causes.Where(c => c.Id == c_id).SingleOrDefault();
            db.Causes.Remove(x);
            db.SaveChanges();
            return Json("OK");
        }

        //Method that disables the sign cause button in case the user has already signed.
        [HttpPost]
        public ActionResult CheckSignature(string cid)
        {
            if (Session["User"] != null)
            {
                int c_id = Convert.ToInt32(cid);
                Cause x = db.Causes.Where(c => c.Id == c_id).SingleOrDefault();
                int uid = ((User)Session["User"]).Id;
                if (x.SignedBy.Contains(uid))
                {
                    return Json("Yes");
                }
            }
            return Json("No");
        }

        //Log out method, unsets session variable.
        [HttpPost]
        public ActionResult LogOut()
        {
            Session["User"] = null;
            return Json("OK");
        }

        //Return Views
        public ActionResult startCause()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        //Returns browse view with a list of causes
        public ActionResult Browse()
        {
            return View(db.Causes.ToList());
        }

        //Returns browse view with a refined list of causes
        public ActionResult BrowseSearch(string search)
        {
            List<Cause> list = new List<Cause>();
            foreach (Cause c in db.Causes)
            {
                if (c.Title.Contains(search.Trim()))
                {
                    list.Add(c);
                }
            }
            return View("Browse", list);
        }

        //Load partial views for use by modals in JS
        public ActionResult LoginPV()
        {
            return PartialView("_LoginPage");
        }

        public ActionResult RegisterPV()
        {
            return PartialView("_RegisterPage");
        }
    }
}