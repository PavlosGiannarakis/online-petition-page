﻿using System.Web;
using System.Web.Mvc;

namespace PFW_CW_40110403
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
